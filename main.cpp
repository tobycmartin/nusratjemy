#include <vector>
#include <assert.h>

using namespace std;

int myFunction(vector<int> vector1, vector<int> vector2)
{
    int result = 0;
    int value;
    for (auto x : vector1)
        {
            for (auto y : vector2)
                {
                    if (y < x)
                    {
                        value = 0;
                        break;
                    }
                    value = 1;
                }
            result += value;
        }
    return result;
}
int main()
{
assert(myFunction(vector<int>{1, 2, 3}, vector<int>{2, 3, 4}) == 2);
return 0;
}

